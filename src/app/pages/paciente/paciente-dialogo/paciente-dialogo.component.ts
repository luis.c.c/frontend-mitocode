import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-paciente-dialogo',
  templateUrl: './paciente-dialogo.component.html',
  styleUrls: ['./paciente-dialogo.component.css']
})
export class PacienteDialogoComponent implements OnInit {

  paciente: Paciente;

  constructor(
    private dialogRef: MatDialogRef<PacienteDialogoComponent>,
    //@Inject(MAT_DIALOG_DATA) private data: Paciente,
    private pacienteService: PacienteService) { }

  ngOnInit(): void {
    this.paciente = new Paciente();
    this.paciente.idPaciente=0;
    /* this.paciente.idPaciente=this.data.idPaciente;
     this.paciente.apellidos=this.data.apellidos;
     this.paciente.dni=this.data.dni;
     this.paciente.email=this.data.email;
     this.paciente.nombres=this.data.nombres;
     this.paciente.telefono=this.data.telefono;*/
  }
  operar() {
    this.pacienteService.registrar(this.paciente).subscribe((data:Paciente) => {
      this.pacienteService.setPacienteSeleccion(data);
      this.pacienteService.setMensajeCambio('SE REGISTRO PACIENTE');
      this.dialogRef.close();
    });

  }
  cerrar() {
    this.dialogRef.close();
  }
}
