import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { SignoVital } from 'src/app/_model/signoVital';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignoVitalService } from 'src/app/_service/signovital.service';
import { PacienteDialogoComponent } from '../../paciente/paciente-dialogo/paciente-dialogo.component';

@Component({
  selector: 'app-signo-vital-edicion',
  templateUrl: './signo-vital-edicion.component.html',
  styleUrls: ['./signo-vital-edicion.component.css']
})
export class SignoVitalEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  pacientes: Paciente[];

  myControlPaciente: FormControl = new FormControl();
  pacientesFiltrados$: Observable<Paciente[]>;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private signoVitalService: SignoVitalService,
    private pacienteService: PacienteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo_cardiaco': new FormControl(''),
    });
    this.listarPacientes();
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
    this.pacienteService.getPacienteSeleccion().subscribe(data => {
      this.myControlPaciente.setValue(data);
    });
    this.pacienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });
  }
  private initForm() {
    if (this.edicion) {
      this.signoVitalService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSigno),
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo_cardiaco': new FormControl(data.ritmo_cardiaco),
        });
        this.myControlPaciente.setValue(data.paciente)
      });
    } else {

    }
  }
  get f() {
    return this.form.controls;
  }
  operar() {
    if (this.form.invalid) { return; }

    let signoVital = new SignoVital();
    signoVital.idSigno = this.form.value['id'];
    signoVital.paciente = this.myControlPaciente.value;
    // signoVital.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    signoVital.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    signoVital.pulso = this.form.value['pulso'];
    signoVital.ritmo_cardiaco = this.form.value['ritmo_cardiaco'];
    signoVital.temperatura = this.form.value['temperatura'];

    console.log(signoVital);

    if (this.edicion) {
      console.log("editar");
      this.signoVitalService.modificar(signoVital).pipe(switchMap(() => {
        return this.signoVitalService.listar();
      }))
        .subscribe(data => {
          this.signoVitalService.setPacienteCambio(data);
          this.signoVitalService.setMensajeCambio('SE MODIFICO');
        });

    } else {
      console.log("agregar");
      this.signoVitalService.registrar(signoVital).subscribe(() => {
        this.signoVitalService.listar().subscribe(data => {
          this.signoVitalService.setPacienteCambio(data);
          this.signoVitalService.setMensajeCambio('SE REGISTRO');
        });
      });
    }

    this.router.navigate(['signo-vital']);
  }
  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }
  abrirDialogo() {
    this.dialog.open(PacienteDialogoComponent, {
      width: '300px'
    });
  }
}
