import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SignoVital } from 'src/app/_model/signoVital';
import { SignoVitalService } from 'src/app/_service/signovital.service';

@Component({
  selector: 'app-signo-vital',
  templateUrl: './signo-vital.component.html',
  styleUrls: ['./signo-vital.component.css']
})
export class SignoVitalComponent implements OnInit {

  displayedColumns = ['idSigno', 'paciente', 'fecha', 'temperatura', 'pulso', 'ritmo_cardiaco', 'acciones'];
  dataSource: MatTableDataSource<SignoVital>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cantidad: number = 0;

  constructor(
    private signoVitalService: SignoVitalService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.signoVitalService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });

    this.signoVitalService.getSignoVitalCambio().subscribe(data => {
      this.crearTabla(data);
    });
    this.signoVitalService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 3000 })
    });
  }
  crearTabla(data: SignoVital[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }
  mostrarMas(e: any) {
    this.signoVitalService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }
  eliminar(idSigno: number) {
    this.signoVitalService.eliminar(idSigno).pipe(switchMap(() => {
      return this.signoVitalService.listar();
    }))
      .subscribe(data => {
        this.signoVitalService.setPacienteCambio(data);
        this.signoVitalService.setMensajeCambio('SE ELIMINO');
      });

  }


}
