import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from "@auth0/angular-jwt";
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  nombreUsuario: string;
  rol: string;
  constructor(private helper: JwtHelperService) { }

  ngOnInit(): void {
    let token = sessionStorage.getItem(environment.TOKEN_NAME);
    const datos = this.helper.decodeToken(token);

    this.nombreUsuario=datos.user_name;
    this.rol=datos.authorities[1];
  }

}
