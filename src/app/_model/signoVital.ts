import { Paciente } from "./paciente";

export class SignoVital {
  idSigno: number;
  fecha: string; //2020-11-07T11:30:05 ISODate || moment.js
  paciente: Paciente;
  temperatura: string;
  pulso: string;
  ritmo_cardiaco: string;
}
